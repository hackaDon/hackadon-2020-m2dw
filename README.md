# HackaDon 2020 :rocket:

## Lien vers le dépôt GitHub

[n3tx-code/hackadon-instut-engagement](https://github.com/n3tx-code/hackadon-instut-engagement)

## Avant propos

Pour utiliser votre repository, vous aurez besoin de **git**

Avant toute chose donc, récuperez votre repository avec la commande suivante :

```sh
git clone git@framagit.org:hackaDon/hackadon-2020-m2dw.git
```

Dans ce repository il y a trois dossiers **html**, **gatsby** et **api-platform**
ces différents dossiers vous permettent si vous le désirez de créer de la documentation et un support interactif pour votre présentation.


## **Le dossier html**

Ce dossier vous permet de créer un site de manière simple, ajoutez vos pages html / assets etc ...

La publication de votre site s'effectuera ici  : https://hackaDon.frama.io/hackadon-2020-m2dw/html/

Pour ce faire, c'est très simple :
* Faire un push sur la branche master
    * C'est l'intégration continue (gitlab-ci.yml) qui s'occupe de la publication automatique.




## **Le dossier gatsby**

Gatsby nous permet de générer un site statique rapidement.

Dans ce dossier vous avez donc à disposition un **boiler-plate** vous permettant de créer une documentation / présentation de votre travail
de manière interractive.

Vous pourrez utiliser des composants **react** si vous le désirez grâce au mdx, vous aurez aussi une architecture de site pré établie.

Pour utiliser ce boiler-pate vous aurez besoin de docker et docker-compose

- [docker](https://docs.docker.com/install/)
- [docker-compose](https://docs.docker.com/compose/install/)

Une fois ces outils installés, executez la commande suivante :

```sh
docker-compose up
```

L'installation du site devrait prendre quelques secondes.

Rendez-vous ensuite dans votre navigateur à l'adresse `http://localhost:1111/`

Et voilà !

![boiler-plate](gatsby.png "gatsby")

Modifiez à votre convenance les fichiers dans le dossier gatsby, plus d'information ici : [hackadon-gatsby](https://hackadon.frama.io/hackadon/Les%20bases%20de%20Gatsby/1-Gatsby)

La publication de votre site s'effectuera ici  : https://hackaDon.frama.io/hackadon-2020-m2dw/

Pour ce faire, c'est très simple :
* Faire un push sur la branche master
    * C'est l'intégration continue (gitlab-ci.yml) qui s'occupe de la publication automatique.



## Le dossier api-platform pour un développement plus poussé

Vous avez choisi une application web/api comme projet ? Ça tombe bien ! Le dossier api-platform est votre dossier de développement prêt à l'emploi.

Attention : api-platform ne sera accessible qu'en local, n'hésitez pas à documenter votre présentation en vous appuyant sur ce que vous faites avec api-platform

Pour utiliser api-platform vous devrez utiliser docker et docker-compose

- [docker](https://docs.docker.com/install/)
- [docker-compose](https://docs.docker.com/compose/install/)

Une fois ces outils installés, rien de plus simple, déplacez vous dans le dossier api-platform

```sh
cd api-platform
```

Puis executez les commandes suivantes :

```sh
docker-compose pull # Download the latest versions of the pre-built images
docker-compose up -d # Running in detached mode
```

Suite à cela, en local, vous aurez :

* Une api : https://localhost:8443/
* Une admin (pour CRUD vos données) : https://localhost:444
* Un client : https://localhost/

Ensemble de documentations utiles :

* Api Platform : [https://api-platform.com/docs/](https://api-platform.com/docs/)
* Docker :  [https://api-platform.com/docs/](https://www.docker.com/)

**À vous de jouer ! :D**